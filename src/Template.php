<?php

namespace Dzion;

class Template
{
    protected $templatePath;
    protected $content;
    protected $data;
    protected $file;

    public function __construct(string $templatePath) {
        $this->templatePath = $templatePath;
    }

    public function render(string $file, array $data = []) : self {
        $this->file = $file;
        $this->data = $data;
        extract($data);
        ob_start();
        include $this->templatePath . '/index.php';
        $this->content = ob_get_contents();
        ob_get_clean();
        return $this;
    }

    public function json(array $data) {
        $data = json_encode($data, JSON_PRETTY_PRINT | JSON_PRETTY_PRINT);
        die($data);
    }

    public function getContent() {
        echo $this->content;
    }

}