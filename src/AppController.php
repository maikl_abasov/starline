<?php

namespace Dzion;

class AppController
{
    protected $db;

    public function __construct(Database $db)
    {
        $this->db = $db;

        // $this->checkAuth();
    }

    public function getMaterials()
    {
        return [];
    }

    public function getMaterialSearch(array $form) : array
    {

        $filters = [];

        foreach ($form['like'] as $name => $value) {
            if (empty($value)) continue;
            if(is_string($value)) {
                $filters[] = "`$name` LIKE '%$value%'";
            }
        }

        foreach ($form['between'] as $name => $item) {
            if (empty($item)) continue;

            $item = (array)$item;
            $between = [];
            foreach ($item as $param) {
                $param = (array)$param;
                if(empty($param['by'])) {
                    $filters[] = "`$name` LIKE '%" .$param['with']. "%'";
                } else {
                    $between[] = " (`$name` BETWEEN " .$param['with'].  " AND " . $param['by'] . ") ";
                }
            }

            if(!empty($between)) {
                $filters[] = " (" . implode(' OR ', $between) . ") ";
            }

        }

        $limit = '';
        if(!empty($form['page'])) {
            $formPage = (array)$form['page'];
            $pageNum = $formPage['page_num'];
            $limit = $formPage['limit'];
            $offset = ($pageNum - 1) * $limit;
            $limit = (isset($offset) & isset($limit)) ? " LIMIT $offset, $limit" : '';
        }

        $condition = (!empty($filters)) ? " WHERE " . implode(' AND ', $filters) : '';

        $sql = "SELECT * FROM materials " . $condition . $limit;

        $result = $this->db->query($sql);

        return $result;
    }

    public function materialSearchFieldItems(string $fname, string $value) : array
    {
        $sql = "SELECT `$fname` FROM materials WHERE `$fname` LIKE '%$value%' ORDER BY `$fname` ASC LIMIT 40";
        $result = $this->db->query($sql);
        return $result;
    }

    public function post(): array
    {
        $data = file_get_contents('php://input');
        return (array)json_decode($data);
    }

    public function saveData(string $file) : array
    {

        $result = [];
        $data = file($file);

        foreach ($data as $i => $line) {
            if($i == 0) continue;

            $line = trim($line);
            $line = str_replace("  ", " ", $line);
            $item = explode(" ", $line);
            $name = '';
            foreach ($item as $key => $value) {
                if ($key > 4) {
                    $name .= ' ' . $value;
                    unset($item[$key]);
                }
            }

            $item[5] = $name;

            $sql = "INSERT INTO materials (`number`, `created_dt`, `group`, `user`, `vendor`, `name`)
                    VALUES ('" . $item[0]. "', '" . $item[1]. "', '" . $item[2]. "', '" . $item[3]. "', '" . $item[4]. "', '"
                          . iconv("Windows-1251", "UTF-8", $item[5]) . "');";

            $this->db->exec($sql);
            $result[] = $item;
        }

        return $result;
    }

    public function auth(string $email, string $password) : bool
    {

        $status = false;
        $password = md5($password);
        $sql = "SELECT * FROM users WHERE `email` = '$email' AND `password` = '$password' ";
        $user = $this->db->query($sql);

        if(!empty($user[0])) {
            $status = true;
            $user = $user[0];
            setcookie('auth_state', $password , time()+6600);
            $this->redirect('material');
        }

        return $status;
    }

    public function checkAuth()
    {
        if(empty($_COOKIE['auth_state'])) {
            $this->redirect('/login-form');
        }
    }

    public function redirect($page)
    {
//        $self = explode('/', trim($_SERVER['PHP_SELF'], '/'));
//        $self[2] = $page;
//        $url = implode('/', $self);
        $url = str_replace("index.php", "", $_SERVER['REQUEST_URI']);
        // lg($_SERVER);
        header("Location:" . $page);
        exit;
    }

}