<?php

namespace Dzion;

use PDO;

class Database
{

    private PDO $pdo;
    private array $config;

    public function __construct(array $config)
    {
        $this->config = $config;
        $this->connection();
    }

    private function connection() : void
    {
        $driver   = $this->config['driver'];
        $dbname   = $this->config['dbname'];
        $user     = $this->config['user'];
        $password = $this->config['password'];
        $host     = $this->config['host'];
        $port     = $this->config['port'];

        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        $dsn = $driver . ':host=' .$host. ';dbname=' . $dbname . ';port=' . $port;
        $this->pdo = new PDO($dsn, $user, $password, $options);

    }

    // Операции над БД
    public function query($sql, $params = [])
    {
        $stmt = $this->pdo->prepare($sql); // Подготовка запроса
        // Обход массива с параметрами и подставление значений
        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $stmt->bindValue(':' .$key, $value);
            }
        }
        $stmt->execute(); // Выполняем запрос
        return $stmt->fetchAll(PDO::FETCH_ASSOC);// Возвращаем ответ
    }

    public function insert(array $data, string $table) : int | bool {
        $response = null;
        try {
            $sql = "INSERT INTO {$table} " . $this->insertSqlPrepare($data);
            // lg([$sql, $data]);
            $stmt= $this->pdo->prepare($sql);
            $response = $stmt->execute($data);
            if($response) $response = $this->pdo->lastInsertId();
        } catch (\PDOException $exception) {
            lg($exception->getMessage());
            throw new \Exception($exception->getMessage(), $exception->getCode());
        }
        return $response;
    }

    public function exec(string $sql) {
        $resp = null;
        try {
            $resp = $this->pdo->exec($sql);
        } catch (\PDOException $exception) {
            throw new \Exception($exception->getMessage(), $exception->getCode());
        }
        return $resp;
    }

    public function insertSqlPrepare(array $data) : string {
        $fields = $values = [];
        foreach ($data as $fname => $value) {
            $fields[] = $fname;
            $values[] = ':' . $fname;
        }
        return  " (" .implode(',', $fields). ") VALUES (".implode(',', $values).") ";
    }

    public function update(array $data, string $table, array $where) : int | bool {
        $response = null;
        try {
            $fields = $this->updateSqlPrepare($data);
            $condition = $this->updateSqlPrepare($where);
            foreach ($where as $key => $value) $data[$key] = $value;

            $sql = "UPDATE {$table} SET {$fields} WHERE " . $condition;
            $this->pdo->prepare($sql)->execute($data);
        } catch (\PDOException $exception) {
            throw new \Exception($exception->getMessage(), $exception->getCode());
        }
        return $response;
    }

    public function updateSqlPrepare(array $data) : string {
        $fields = [];
        foreach ($data as $fname => $value) {
            $fields[] = "$fname=:$fname";
        }
        return  implode(',', $fields);
    }

    protected function getParamType($param)
    {
        switch(strtolower(gettype($param))) {
            case "boolean": return \PDO::PARAM_BOOL;
            case "integer": return \PDO::PARAM_INT;
            case "null": return \PDO::PARAM_NULL;
            default: return \PDO::PARAM_STR;
        }
    }

    protected  function transaction($callback) : array
    {

        $this->pdo->beginTransaction();
        try {
            $result = call_user_func($callback);
            $this->pdo->commit();
            return $result;
        } catch(Exception $exception) {
            $this->pdo->rollback();
            throw new \Exception('DATABASE: ' .$exception->getMessage() . ' ! (beginTransaction)');
        }
    }

    public function getPdo() {
        return $this->pdo;
    }

}
