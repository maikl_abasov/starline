<?php

error_reporting(E_ALL);
ini_set("display_error", true);
ini_set("error_reporting", E_ALL);

session_start();

define('APP_PATH', dirname(__DIR__));
define('TEMPLATE_PATH', APP_PATH . '/public/template');

require APP_PATH . '/vendor/autoload.php';

use Dzion\AppController;
use Dzion\Template;
use Dzion\Database;
use Bramus\Router\Router;

$config = [
    'host'     => 'mysql_db',
    'driver'   => 'mysql',
    'dbname'   => 'starline',
    'user'     => 'root',
    'password' => '1234',
    'port'     => 3306
];

try {
    $database = new Database($config);
} catch (Exception $exception) {
    die($exception->getMessage());
}

$router = new Router();

$router->get('/logout', function() use ($database)  {
    setcookie( "auth_state", "", time()- 60);
    (new AppController($database))->redirect('login-form');
});

$router->post('/user-auth', function() use ($database)  {
    $state = (new AppController($database))->auth($_POST['email'], $_POST['password']);
    $template = new Template(TEMPLATE_PATH);
    $template->render('auth', [$state])->getContent();
});

$router->get('/login-form', function() use ($database)  {
    $template = new Template(TEMPLATE_PATH);
    $template->render('auth', [])->getContent();
});

$router->get('/', function() use ($database)  {
    $appController = new AppController($database);
    $appController->checkAuth();
    $data = $appController->getMaterials();
    $template = new Template(TEMPLATE_PATH);
    $template->render('home', $data)->getContent();
});

$router->get('/material', function() use ($database)  {
    $appController = new AppController($database);
    $appController->checkAuth();
    $data = $appController->getMaterials();
    $template = new Template(TEMPLATE_PATH);
    $template->render('home', $data)->getContent();
});

$router->post('/material/search', function() use ($database)  {
    $appController = new AppController($database);
    $appController->checkAuth();
    $form = $appController->post();
    $data = $appController->getMaterialSearch($form);
    (new Template(TEMPLATE_PATH))->json($data);
});

$router->post('/material/field/search-items', function() use ($database)  {
    $appController = new AppController($database);
    $appController->checkAuth();
    $form = $appController->post();
    $data = $appController->materialSearchFieldItems($form['field'], $form['value']);
    (new Template(TEMPLATE_PATH))->json($data);
});

$router->get('/material/export', function() use ($database)  {
    $data = (new AppController($database))->saveData(APP_PATH . '/public/export.csv');
    lg($data);
});

$router->get('/material/test', function() use ($database)  {
    $form = getForm();
    // lg($form);
    $data = (new AppController($database))->getMaterialSearch($form);
    lg($data);
});

$router->run();


///////////////////
///
///
///

function lg($data) {
    $data = print_r($data, true);
    die("<pre>$data</pre>");
}

function getForm() {
    $form = '
        {
          "like": {
            "number": "",
            "created_dt": "",
            "group": "11",
            "user": "",
            "vendor": "",
            "name": ""
          }
          ,"between": {
            "number": [{ "with": "000000000003100202", "by": "000000000003100207" },
                       { "with": "000000000003100258", "by": "000000000003100267" }],
            "created_dt": []
          }
          ,"page":{"page_num":"2", "limit":"3"}
        }
    ';
    return (array)json_decode($form);
}

//{ "with": "000000000003100258", "by": "000000000003100267" }


