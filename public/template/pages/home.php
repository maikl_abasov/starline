<div>

    <div style="display: flex">

        <a @click="getMaterialItems()" href="#" class="double-border-button"
           style="width: 200px; margin-top: 10px"> Получить отчёт </a>

        <div class="paginator-box">
            <button @click="prevPage()">Prev</button>
            <button @click="nextPage()" >Next</button>

            <div>Кол / страниц: {{pageCount}}</div>
        </div>


        <a @click="clearAllFilters()" href="#" class="double-border-button"
           style="width: 200px; margin:10px 10px 0px  auto;"> Очистить все фильтры </a>

        <div v-if="itemsCount" style="padding: 15px 10px 0px 0px">Кол. {{itemsCount}}</div>
    </div>

    <table class="table_dark">
        <tr>
            <th>ID</th>
            <th>Материал (номер)</th>
            <th>Наименование</th>
            <th>Дата создания</th>
            <th>Группа</th>
            <th>Имя пользователя</th>
            <th>Вид материала</th>
        </tr>

        <tr>
            <td></td>
            <td style="">
                <div style="display: flex">
                    <input v-model="form.like.number" type="text" class="search-input"
                           style="margin-right: 5px !important;">
                    <form-modal
                        title="'Материал (номер)'"
                        fname="number"
                        @set_filter="setFilter"
                    />
                </div>
            </td>

            <td style="">
                <div style="display: block">
                    <input v-model="form.like.name"
                           @input="searchFieldItems('name', form.like.name)" type="text" class="search-input"><br>

                      <div class="datalist-container" id="datalist-name">
                        <div v-for="(item) in fieldItems"
                             @click="setName(item)">
                            {{item.name}}
                        </div>
                      </div>

                </div>
            </td>

            <td style="">
                <div style="display: flex">
                    <input v-model="form.like.created_dt" type="text" class="search-input"
                           style="margin-right: 5px !important;" >
                    <form-modal
                            title="'Дата создания'"
                            fname="created_dt"
                            @set_filter="setFilter"
                    />
                </div>
            </td>

            <td style="">
                <div style="display: flex">
                    <input v-model="form.like.group" type="text" class="search-input">
<!--                    <a href="" class="double-border-button"> ext </a>-->
                </div>
            </td>

            <td style="">
                <div style="display: flex">
                    <input v-model="form.like.user" type="text" class="search-input">
<!--                    <a href="" class="double-border-button"> ext </a>-->
                </div>
            </td>

            <td style="">
                <div style="display: flex">
                    <input v-model="form.like.vendor" type="text" class="search-input">
<!--                    <a href="" class="double-border-button"> ext </a>-->
                </div>
            </td>

        </tr>

        <tr v-for="(item) in materialItems">
            <td>{{item.id}}</td>
            <td>{{item.number}}</td>
            <td>{{item.name}}</td>
            <td>{{item.created_dt}}</td>
            <td>{{item.group}}</td>
            <td>{{item.user}}</td>
            <td>{{item.vendor}}</td>
        </tr>

    </table>

<!--    <pre>{{form}}</pre>-->
<!--    <pre>{{materialItems}}</pre>-->

</div>

