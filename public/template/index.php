
<!doctype html>
<html>
<head>

    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1,minimal-ui" name="viewport">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic|Material+Icons">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/vuetify@3.3.13/dist/vuetify.min.css">

    <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <link rel="stylesheet" href="/template/style.css">

    <style></style>

</head>

<body>
<div id="app">
    <v-layout class="rounded rounded-md">

        <v-app-bar title="StarLine">

            <?php if(!empty($_COOKIE['auth_state'])) { ?>
                <a href="logout" style="margin-right: 10px" > Выход </a>
            <?php } ?>
            
        </v-app-bar>

        <v-main class="d-flex align-center justify-center" style="border: 0px red solid">
            <?php  include $this->templatePath . '/pages/' . $this->file . '.php'; ?>
        </v-main>

    </v-layout>
</div>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.4/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vuetify@3.3.13/dist/vuetify.min.js"></script>

<script src="/template/components/form.modal.js"></script>
<script src="/template/components/auth.js"></script>
<script src="/template/components/app.js"></script>

<script type="module" >

    const {createApp} = Vue;
    const {createVuetify} = Vuetify;
    const vuetify = createVuetify();

    const app = createApp(appConfig);
    app.use(vuetify);
    app.mixin(BaseMixin);
    app.component('form-modal', FormModal);
    app.component('auth-form', AuthForm);

    app.mount('#app');

</script>

</body>
</html>