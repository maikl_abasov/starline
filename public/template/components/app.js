
const BaseMixin = {

    methods: {
        async send(url, data = null) {
            const response = (data) ?
                await axios.post(url, data) : await axios.get(url);
            return response.data;
        },
    }
}

const appConfig = {
    data() {
        return {
            itemsCount: 0,
            fieldItems: [],
            materialItems: [],
            pageCount: 0,
            form: {
                like: {
                    number: null,
                    created_dt: null,
                    group: '',
                    user: '',
                    vendor: '',
                    name: ''
                },

                between: {
                    number: null,
                    created_dt: null,
                },
                page: { page_num: 1, limit: 6 },
            }
        }
    },

    methods: {

        getMaterialItems() {
            const url = 'material/search';
            this.send(url, this.form).then(response => {
                this.materialItems = response;
                this.itemsCount = this.materialItems.length;
                // this.pageCount = (parseInt(this.itemsCount) / parseInt(this.form.page.limit));
            })
        },

        setFilter(data) {
            this.form.like[data.fname] = '';
            this.form.between[data.fname] = data.list;
            this.getMaterialItems();
        },

        searchFieldItems(field, value) {
            const url = 'material/field/search-items';
            this.send(url, { field, value }).then(response => {
                $("#datalist-name").show();
                this.fieldItems = response;
            })
        },

        setName(item) {
            this.form.like['name'] = item.name;
            $("#datalist-name").hide();
        },

        clearAllFilters() {

            const like = {
                number: null,
                created_dt: null,
                group: '',
                user: '',
                vendor: '',
                name: ''
            };

            const between = {
                number: null,
                created_dt: null,
            };

            const page = { page_num: 1, limit: 6 };

            this.form = { like, between, page}

            this.getMaterialItems();
        },

        prevPage() {
            let page = this.form.page.page_num;
            if(parseInt(page) < 1 || page == 1) page = 1;
            else page--;
            this.form.page.page_num = page;
            this.getMaterialItems();
        },

        nextPage() {
            let page = this.form.page.page_num;
            if(parseInt(page) < 1) page = 1;
            else page++;
            this.form.page.page_num = page;
            this.getMaterialItems();
        },

    },
}