
const FormModal = {

    emits: ['set_filter'],
    props: ['fname', 'title'],

    data() {
        return {
            list: [],
            //item: {with: '000000000003100202', by: '000000000003100207'},
            //item: {with: '000000000003100258', by: '000000000003100267'},
            //item: {with: '20180314', by: '20180603'},
        }
    },

    created() {
        this.init();
    },

    methods: {

        init() {
            let list = this.getList();
            if(list && list.length) this.list = list;
            else this.addItem();
        },

        emitOk() {

            let fname = this.fname;
            let list  = [];
            let like  = [];
            for (let i in this.list) {
                let item = this.list[i];
                if(item.with) {
                    list.push(item);
                    // if(item.by) list.push(item)
                    // else like.push(like)
                }
            }

            if(list.length > 0)  {
                $("#btn-" + this.fname).addClass("green-btn");
            } else {
                $("#btn-" + this.fname).removeClass("green-btn");
            }

            this.$emit('set_filter', { list , fname, like } );
        },

        emitClear() {
            this.list = [];
            this.list.push({ with: '', by: '' });
        },

        emitCancel() {
        },

        addItem() {
            this.list.push({ with: '', by: '' });
        },

        getList() {
            return this.$root.form.between[this.fname]
        },

    },

    template: `
           <div ><v-dialog width="500">

                <template v-slot:activator="{ props }">
                    <v-btn v-bind="props" text="ext" :id="'btn-' + fname" > </v-btn>
                </template>

                <template v-slot:default="{ isActive }"><v-card :title="'Расширенный фильтр для ' + title">

                        <v-card-text class="form-modal" >

                             <div style="font-style: italic">Значение или диапазон значений</div>

                             <table style="width: 100% !important;">

                               <tr>
                                  <th style="text-align: center"> С </th>
                                  <th style="text-align: center"> По </th>
                               </tr>

                               <tr v-for="(item) in list" >
                                  <td><input v-model="item.with" type="text" ></td>
                                  <td><input v-model="item.by" type="text" ></td>
                               </tr>

                             </table>

                        </v-card-text>

                        <v-card-actions class="form-modal-btn">

                            <v-spacer></v-spacer>

                            <v-btn text="+" @click.prevent="addItem();"></v-btn>
                            <v-btn text="Ок" @click.prevent="emitOk(); isActive.value = false"></v-btn>
                            <v-btn text="Очистить" @click="emitClear"></v-btn>
                            <v-btn text="Отмена" @click="emitCancel(); isActive.value = false"></v-btn>

                        </v-card-actions>

                </v-card></template>
            </v-dialog></div>
        `,
}
